var Bicicleta = require("../../models/bicicleta");
var request = require('request');

var server = require('../../bin/www');
var mongoose = require('mongoose');
var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeAll(async () => {
        await mongoose.disconnect();
    });


    beforeAll(function (done) {

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true }, function () {
            done();
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('we are connected to test database');
        });

    });


    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            //mongoose.disconnect(err)
            done();
        });
    });


    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (err, resp, body) {
                var result = JSON.parse(body);
                expect(resp.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);

                done();
            });
        });
    });

    describe('POST bibiclesas /create', () => {
        it('status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"code":10,"color":"gris","modelo":"cerokm","lat":-33.443,"long":-33.424 }';
            
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    console.log(bici);
                    expect(bici.color).toBe("gris");
                    expect(bici.modelo).toBe("cerokm");
                    expect(bici.ubicacion[0]).toBe(-33.443);
                    expect(bici.ubicacion[1]).toBe(-33.424);
                    done();
            });
        }); 
    });
    describe('UPDATE Bicicletas /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var bici = '{ "code": 3, "color": "azul", "modelo": "urbana", "lat": 3.4760683, "lng": -76.4887271 }';

            var a = new Bicicleta({code: 3,color: 'rojo',modelo: 'montana',ubicacion: [3.4693968, -76.4887123]});
            Bicicleta.add(a, function (err, newBici) {
                request.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/update',
                    body: bici
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(204);
                    Bicicleta.findByCode(3, function (err, targetBici) {
                        console.log(targetBici);
                        expect(targetBici.color).toBe('azul');
                        expect(targetBici.modelo).toBe('urbana');
                        expect(targetBici.ubicacion[0]).toBe(3.4760683);
                        expect(targetBici.ubicacion[1]).toBe(-76.4887271);

                        done();
                    });
                });
            });
        });
    });
    /*describe('POST bibiclesas /update', () => {
        it('status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"code":10,"color":"marronazo","modelo":"viejardo","lat":-33.443,"long":-33.424 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: aBici
            }, function (error, response, body) {
                expect(Bicicleta.findByCode(10).color).toBe('marronazo');
                expect(response.statusCode).toBe(204);
                console.log("Hecho por diego fuentes https://bitbucket.org/diegoatnqn/curso-server/src/master/");
                done();
            });
        });
    });*/

    describe('POST bibiclesas /delete', () => {
        it('status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code": 10 }';
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(204);
                    console.log("Hecho por diego fuentes https://bitbucket.org/diegoatnqn/curso-server/src/master/");
                done();
            });
        });
    });
});