var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");



describe('Testing Bicicletas', function () {
    beforeAll(async () => {
        await mongoose.disconnect();
    });

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true })
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una nueva bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "calle", [-34.5, -34.6]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("calle");
            expect(bici.ubicacion[1]).toEqual(-34.6);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('crea una nueva bicicleta', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "marron", modelo: "negra" });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);
                    done();
                    });
                });
            });
        });
    

    describe('Bicicleta.findByCode', () => {
        it('debe devolver bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);   
                var aBici = new Bicicleta({ code: 1, color: "amarillo", modelo: "rojo" });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    var aBici4 = new Bicicleta({ code: 2, color: "naranja", modelo: "RIO" });
                    Bicicleta.add(aBici4, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
    });
    });
});

/*
beforeEach(() => { Bicicleta.allBicis = []; });
describe('Bicicleta.allBicis', () => {
    it('Comienza vacio', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var nueva = new Bicicleta(1, "azul", "2020", [-33.33, -34.33]);
        Bicicleta.add(nueva);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(nueva);
    });
});

describe('Bicicleta.findById', () => {
    it('devolver bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "azul", "marino", [-22.222, -22.222]);
        var aBici2 = new Bicicleta(2, "azulad", "marinad", [-22.333, -22.333]);

        Bicicleta.add(aBici);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});

describe('Bicicleta.removeById', () => {
    it('eliminar bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "azul", "marino", [-22.222, -22.222]);
        Bicicleta.add(aBici);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})*/