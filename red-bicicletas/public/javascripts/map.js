var map = L.map('myMap', {
    center: [-38.95161, -68.0591],
    zoom: 13
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map);

var marker = L.marker([-38.950, -68.09]).addTo(map);
var marker = L.marker([-38.900, -67.999]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(function (bici) {
            var marker = L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
        })
    }
})